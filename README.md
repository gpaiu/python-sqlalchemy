# python-sqlalchemy

Introduction to SQLAlchemy and RDBMS.

This script starts a webserver and a database connection. Each time a GET request is made,
an entry is added to a database table (```access_log```), and as an HTTP response, you
will receive the 30 last access points in the form of ```SERVER HOSTNAME - ACCESS TIME```

## CLI Options

- ``host``. Default: **0.0.0.0**

    Bind address of the webserver. 

- ``port``. Default: **8080**

    Bind port.

- ``db_host``. Default: **localhost**

    Database host to connect to.

- ``db_port``. Default: **3306**

    Database server port.

- ``db_user``. Default: **sandbox**

    Database user.

- ``db_password``. Default: **sandbox**

    Database password.

- ``db_name``. Default: **sandbox**

    Database name.

## Deploying to Minikube

The kubernetes objects define several items that may or may not be needed, but to make this work
end to end, the following objects need to be adjusted
- `Secret/gitlab-registry` - contains a reference to the deploy token used in `imagePullSecrets`.
If you are building this image locally and use a trusted registry with no authentication, you will
not need it, nor `imagePullSecrets` key in the container template spec. If you are using another
trusted registry, make sure to set the `.dockerconfigjson` key accordingly.
- `Service type: Loadbalancer` is used because `minikube tunnel` takes care of the rest. The
deployment/service can be exposed differently - for instance using `kubectl port-forward` or using a `NodePort Service`
but the process has some caveats, so LB is preferred.
