#!/bin/bash

function envsubst_files_in_place() {
    for file in $@; do
        if [ "$ENVSUBST_VAR_STRING" ]; then
          rendered_output="$(envsubst \'$ENVSUBST_VAR_STRING\' < $file)"
        else
          rendered_output="$(envsubst < $file)"
        fi
        echo "$rendered_output" > $file
    done
}

GITLAB_DEPLOY_USER=$(cat .secrets/gitlab-user)
GITLAB_DEPLOY_TOKEN=$(cat .secrets/gitlab-token)

DOCKER_CONFIG_JSON=$(echo -n "$GITLAB_DEPLOY_USER:$GITLAB_DEPLOY_TOKEN" | base64)
export DOCKER_CONFIG_JSON
envsubst_files_in_place kubernetes-objects/2-gitlab-deploy-token.yml
