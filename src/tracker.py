#!/usr/bin/env python3
"""Track hostnames and store them into a MariaDB databgase"""
# pylint: disable=R0913, C0103

import argparse
import datetime
import json
import os
import platform
import psutil
import signal
import socket
import time

from collections import OrderedDict

import kubernetes
import redis

from http.server import HTTPServer, BaseHTTPRequestHandler
from jinja2 import Environment, FileSystemLoader
from sqlalchemy import (
    Column,
    MetaData,
    Table,
    String,
    create_engine,
    desc,
    insert,
    select,
)

class DB:
    """Wrapper over a RDBMS"""

    def __init__(self, server, port, user, password, database, dialect="mysql+pymysql"):
        """Constructor"""

        self._engine = create_engine(
            "%s://%s:%s@%s:%d/%s"
            % (
                dialect,
                user,
                password,
                server,
                port,
                database,
            )
        )
        self._db = None

        # Define table(s)
        self._metadata = MetaData()
        self._access_log = Table(
            "access_log",
            self._metadata,
            Column("hostname", String(128), nullable=False),
            Column("access_time", String(128), nullable=False),
        )

    def init(self):
        """Establish DB connection and create tables"""
        self._db = self._engine.connect()
        self._metadata.create_all(self._engine)

    def insert_record(self):
        """INSERT INTO access_log VALUES"""
        query = insert(self._access_log).values(
                hostname=socket.gethostname(),
                access_time=datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S"),
                )
        self._db.execute(query)
        return self._db.commit()

    def get_records(self, limit=30):
        """SELECT * FROM access_log LIMIT limit ORDER BY access_time DESC"""
        query = select(self._access_log).order_by(desc(self._access_log.c.access_time)).fetch(limit)
        result_proxy = self._db.execute(query)
        return result_proxy.fetchall()

    def close(self):
        """Close the connection"""
        self._db.close()


class CustomHTTPServer(HTTPServer):

    alive = True

    def __init__(self, *args, **kwargs):
        """Initialize DB connection"""
        self._redis = redis.Redis(host=self.cfg.redis_host, port=self.cfg.redis_port, decode_responses=True)

        try:
            system_ip_address = socket.gethostbyname(socket.gethostname())
        except socket.gaierror:
            system_ip_address = "N/A"

        self._system_data = {
            'ip': system_ip_address,
            'distribution': platform.uname().system,
            'architecture': platform.uname().machine,
            'memory-used':      "%s MB" % round(psutil.virtual_memory().used / (1024 * 1024), 2),
            'memory-available': "%s MB" % round(psutil.virtual_memory().available / (1024 * 1024), 2),
        }
        self._hostname = socket.gethostname()
        super(HTTPServer, self).__init__(*args, **kwargs)

    def _stop(self, signum, frame):
        """ Stop webserver and delete Redis pod record. """
        print("Stopping webserver.")
        CustomHTTPServer.alive = False
        self.server_close()
        self._redis.delete(self._hostname)
    
    def start_listening(self):
        """ Start webserver. Add redis record and trap signals. """
        signal.signal(signal.SIGINT, self._stop)
        signal.signal(signal.SIGTERM, self._stop)
        self._redis.set(self._hostname, json.dumps(self._system_data))
        while CustomHTTPServer.alive:
            self.handle_request()


class CustomHandler(BaseHTTPRequestHandler):
    """BaseHTTPServer handler extended"""

    def __init__(self, *args, **kwargs):
        """Initialize DB connection"""
        self._db = DB(
            self.cfg.db_host,
            self.cfg.db_port,
            self.cfg.db_user,
            self.cfg.db_password,
            self.cfg.db_name,
        )
        # Protected Kubernetes object name prefixes
        self._protected_prefixes = [
            "etcd",
            "kube",
            "kindnet",
            "coredns",
            "extension-apiserver",
            "cluster-info"
        ]
        # Load Redis configuration
        self._redis = redis.Redis(host=self.cfg.redis_host, port=self.cfg.redis_port, decode_responses=True)
        # Load Jinja environment
        self.jinja_env = Environment(loader = FileSystemLoader('views'))
        # Load Kubernetes configuration
        self._k8s = None
        k8s_loaded_config = False
        try:
            kubernetes.config.load_incluster_config()
            k8s_loaded_config = True
        except Exception as e:
            try:
                kubernetes.config.load_kube_config()
                k8s_loaded_config = True
            except Exception:
                print("Could not load Kubernetes cluster configuration")
        if k8s_loaded_config:
            self._k8s = kubernetes.client.CoreV1Api()
        super(CustomHandler, self).__init__(*args, **kwargs)
    
    def _send_response(self, response):
        """ Calculate content-length and send response """
        if not response:
            self._send_response("Empty Response")
            return
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Length", len(response))
        self.end_headers()
        self.wfile.write(bytes(response, "UTF-8"))
    
    def _redirect_to(self, location):
        """ Redirect browser to location"""
        self.send_response(301)
        self.send_header(f'Location', location)
        self.end_headers()
        self.wfile.write(bytes(''), "UTF-8")

    def __render_and_send(self, template, **kwargs):
        """ Render template specified by `template` ref and send back the HTTP response. """

        output = template.render(**kwargs)
        self._send_response(output)

    def do_GET(self):
        """Handle GET requests. Set up mapping and pass calls to methods. """
        match self.path.split('?'):
            case ['/']:
                self.page_home()
            case ['/access_log']:
                self.page_access_log()
            case ['/pods']:
                self.page_pods()
            case ['/k8s']:
                self.page_k8s()
            case ['/podkiller', query_params]:
                self.page_podkiller(params=query_params)
            case _:
                print(f"404: %s", self.path)

    def page_home(self):
        """ Handle home page """
        template = self.jinja_env.get_template('page_home.html')
        self.__render_and_send(template)

    def page_k8s(self):
        """ Handle K8s page """
        template = self.jinja_env.get_template('page_k8s.html')

        kubernetes_objects = {}
        icons = {
            'pod': 'https://github.com/kubernetes/community/blob/master/icons/png/resources/labeled/pod-128.png?raw=true',
            'service': 'https://github.com/kubernetes/community/blob/master/icons/png/resources/labeled/svc-128.png?raw=true',
            'config_map': 'https://github.com/kubernetes/community/blob/master/icons/png/resources/labeled/cm-128.png?raw=true',
        }

        result = self._k8s.list_namespace()
        namespaces = [n.metadata.name for n in result.items]
        for namespace in namespaces:
            # Find Pods, ConfigMaps and Services for each namespace
            try:
              pods = [ x.metadata.name for x in self._k8s.list_namespaced_pod(namespace=namespace).items ]
            except:
              pods = []
            try:
              services = [ x.metadata.name for x in self._k8s.list_namespaced_service(namespace=namespace).items ]
            except:
              services = []
            try:
              config_maps = [ x.metadata.name for x in self._k8s.list_namespaced_config_map(namespace=namespace).items ]
            except:
              config_maps = []
            kubernetes_objects[namespace] = {}
            kubernetes_objects[namespace]['pod'] = pods if pods else []
            kubernetes_objects[namespace]['service'] = services if services else []
            kubernetes_objects[namespace]['config_map'] = config_maps if config_maps else []

        def is_protected(namespace, name):
            """ Helper function to validate protected resources. """
            for prefix in self._protected_prefixes:
                if namespace.lower().startswith(prefix) or name.lower().startswith(prefix):
                    return True
            return False

        self.__render_and_send(template,
                               title = 'Kubernetes',
                               kubernetes_objects=kubernetes_objects,
                               icons=icons,
                               protected_prefixes=self._protected_prefixes,
                               protected=is_protected,
                               )

    def page_access_log(self):
        """ Handle Access Log page """
        template = self.jinja_env.get_template('page_access_log.html')

        self._db.init()
        self._db.insert_record()
        self.__render_and_send(template, title = 'Access Log', records = self._db.get_records())
        self._db.close()
    
    def page_pods(self):
        """ Handle Pods page """
        template = self.jinja_env.get_template('page_pods.html')

        redis_keys = self._redis.keys('*')
        redis_data = OrderedDict()
        for key in redis_keys:
            redis_data[key] = json.loads(self._redis.get(key))
        self.__render_and_send(template, title='Pods', records = redis_data)

    def page_podkiller(self, params):
        resource_identifier = {
            'namespace': None,
            'kind': None,
            'name': None,
        }
        for group in params.split('&'):
            print(group)
            name, value = group.split('=')
            if name in resource_identifier:
                resource_identifier[name] = value
        
        if [ n for n in resource_identifier.values() if n is None ] and resource_identifier['kind'] != 'pod':
            print("Insufficient parameters")
        else:
            print("Calling K8s API for %s/%s" %(resource_identifier['namespace'], resource_identifier['name']))
            self._k8s.delete_namespaced_pod(namespace=resource_identifier['namespace'],name=resource_identifier['name'])
        self._redirect_to('/k8s')

def main():
    """Entrypoint. Parse command line arguments, initialize DB and start the HTTP Server"""

    cli_parser = argparse.ArgumentParser(description="Deployment Server")

    cli_parser.add_argument(
        "--host",
        type=str,
        help="Bind address. Default: 0.0.0.0",
        default=os.environ.get("BIND_ADDRESS", "0.0.0.0"),
        required=False,
    )

    cli_parser.add_argument(
        "--port",
        type=int,
        help="Bind port. Default: 8080",
        default=os.environ.get("BIND_PORT", "8080"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_host",
        type=str,
        help="Database server hostname. Default: localhost",
        default=os.environ.get("DB_HOST", "localhost"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_port",
        type=int,
        help="Database server port. Default: 3306",
        default=os.environ.get("DB_PORT", "3306"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_user",
        type=str,
        help="Database user. Default: sandbox",
        default=os.environ.get("DB_USER", "sandbox"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_password",
        type=str,
        help="Database user password. Default: sandbox",
        default=os.environ.get("DB_PASSWORD", "sandbox"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_name",
        type=str,
        help="Database name. Default: sandbox",
        default=os.environ.get("DB_NAME", "sandbox"),
        required=False,
    )

    cli_parser.add_argument(
        "--redis_host",
        type=str,
        help="Redis name. Default: localhost",
        default=os.environ.get("REDIS_KV_HOST", "localhost"),
        required=False,
    )

    cli_parser.add_argument(
        "--redis_port",
        type=int,
        help="Redis port. Default: 6379",
        default=os.environ.get("REDIS_KV_PORT", 6379),
        required=False,
    )

    cfg = cli_parser.parse_args()

    CustomHandler.cfg = cfg
    CustomHTTPServer.cfg = cfg

    http_server = CustomHTTPServer(
        server_address=(cfg.host, cfg.port), RequestHandlerClass=CustomHandler
    )

    http_server.start_listening()


if __name__ == "__main__":
    main()
